#!/bin/bash

applySegmentation()
{
	filename=$1
	echo "in apply conversion for: $filename"
	purefilename=$(basename "${filename%.*}")
	if ! [[ -f $purefilename ]]; then
		echo "trying"
		../build/bin/MMMBimCaDetection \
			--inputMotion ${filename} 
        fi

}
export -f applySegmentation

targetfolder="../data/Input"

echo $targetfolder
find $targetfolder -name "*.xml" -type f | xargs -I% -P1 bash -c "applySegmentation \"%\" \"$subjectid\""
exit

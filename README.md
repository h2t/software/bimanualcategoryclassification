# Bimanual Category Classification

![](MMMBimanual.PNG)

This repository contains the code to reproduce the results presented in "A Bimanual Manipulation Taxonomy".

The detailed intstructions can be found [here](https://gitlab.com/h2t/software/bimanualcategoryclassification/-/wikis/A-Bimanual-Manipulation-Taxonomy-(2022)).




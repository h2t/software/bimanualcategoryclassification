#include "SlidingClassifier.h"

#include <MMM/Motion/Segmentation/AbstractMotionSegment.h>
#include <MMM/Motion/Segmentation/MotionSegment.h>
#include <MMM/Motion/Segmentation/Segmentation.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Segmentation/MotionRecordingSegment.h>
#include <MMM/Motion/Annotation/ActionLabel/ActionLabelAnnotation.h>
#include <MMM/Motion/Annotation/BimanualLabel/BimanualAnnotation.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <SemanticObjectRelations/Spatial/evaluate_relations.h>
#include <SimoxUtility/math/convert.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/EndEffector/EndEffector.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>
#include <utility>
#include <memory>

using namespace semrel;
using namespace semrel::spatial;
using namespace MMM;

std::string getTimeAsString(){
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::ostringstream oss;
    oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
    return oss.str();
}

SlidingClassifier::SlidingClassifier(MMM::BimScene* scene, BimParameters params)
{
    this->scene = scene;
    this->params = params;
}

void SlidingClassifier::classifyWithSlidingWindow()
{
    // iterate over time and get category for each timestep
    std::vector<double> timesteps;
    for(const auto& timestep : scene->getNodebyName("left_hand")->positions){
        timesteps.push_back(timestep.first);
    }
    categories.clear();
    for(int i = 0; i < (int(timesteps.size())-this->params.windowSize-1); i++){
        BimanualLabel category = SlidingClassifier::classifyWindow(scene, timesteps[i], timesteps[i+this->params.windowSize]);
        categories.push_back(category);
    }
}

void SlidingClassifier::getSegmentation(const MMM::MotionRecordingPtr& motions)
{
    std::vector<double> timesteps;
    for(const auto& timestep : scene->getNodebyName("left_hand")->positions){
        timesteps.push_back(timestep.first);
    }
    int t = 0;
    BimanualLabel prevCategory = BimanualLabel::undefined;
    bool first = true;
    for(const BimanualLabel cat: categories){
        if(first){
            prevCategory = cat;
            segCategories.emplace_back(timesteps[t], prevCategory);
            first = false;
        }
        if(cat!= prevCategory){
            prevCategory = cat;
            segCategories.emplace_back(timesteps[t], prevCategory);
        }
        t++;
    }

    // defragmentation of segmentation
    defragmetSegmentation();

    // create result map
    std::string prevCategoryName;
    std::map<double, std::string> segmentation;
    for(auto & segCategorie : segCategories){
        if(prevCategoryName.empty()){
            prevCategoryName = segCategorie.second._to_string();
            segmentation.insert(std::make_pair(segCategorie.first, prevCategoryName));
        }
        if(segCategorie.second._to_string() != prevCategoryName){
            prevCategoryName = segCategorie.second._to_string();
            segmentation.insert(std::make_pair(segCategorie.first, prevCategoryName));
        }
    }

    // put in MMM segment type
    MMM::MotionRecordingSegmentPtr mosegment(new MMM::MotionRecordingSegment(motions));
    MMM::MotionSegmentationPtr fullSegmentation(new MMM::MotionSegmentation(mosegment, "BimanualCategoryDetection"));
    first = true;
    float prevStart;
    std::string prevName;
    for (const auto& element: segmentation){
        if(first){
            prevStart = element.first;
            prevName = element.second;
            first = false;
        }else{
            MMM::MotionSegmentPtr subseg = fullSegmentation->addSegment(prevStart, element.first);
            auto annotation = std::make_shared<BimanualAnnotation>(BimanualLabel::_from_string(prevName.c_str()));
            subseg->addAnnotation(annotation);
            prevStart = element.first;
            prevName = element.second;

        }

    }
    MMM::MotionSegmentPtr subseg = fullSegmentation->addSegment(prevStart, timesteps.back());
    auto annotation = std::make_shared<BimanualAnnotation>(BimanualLabel::_from_string(prevName.c_str()));
    subseg->addAnnotation(annotation);

    mosegment->addSegmentation(fullSegmentation);
    motions->setMotionSegment(mosegment);



        try {
            motions->saveXML(motions->getOriginFilePath().parent_path().parent_path()/ "OutputData" / (getTimeAsString() + "_" + motions->getOriginFilePath().filename().string()));
        } catch (MMM::Exception::MMMException &e) {
            std::cout << "Saving motion failed: " << e.what() << std::endl;
        }


}

BimanualLabel SlidingClassifier::classifyWindow(const BimScene *scene, float t_min, float t_max) const
{
    BimanualLabel category = BimanualLabel::undefined;
    //
    // unimanual/ bimanual for loose coupling
    // classifies: unimanual_left, unimanual_right, no_motion, loosely_coupled
    //
    bool unimanual_left = false;
    if (scene->getHandGroupInteractionListSize(t_min) == 0){  // no hand group interaction
        Eigen::Vector3f avg_vel_right = Eigen::Vector3f::Zero();;
        int timestep_counter = 0;
        std::vector<MMM::BimNode*> rightGroup = scene->returnCurrentMembersOfGroup(MMM::BimNode::Group::right_hand, t_min);
        std::vector<MMM::BimNode*> leftGroup = scene->returnCurrentMembersOfGroup(MMM::BimNode::Group::left_hand, t_min);
        MMM::BimNode* right;
        for(MMM::BimNode* node: rightGroup){
            if(node->name == "right_hand"){
                right = node;
            }
        }
        for(const auto& timestep : right->velocities){
            if(timestep.first >= t_min && timestep.first < t_max){
                avg_vel_right += timestep.second;
                timestep_counter++;
            }
        }
        avg_vel_right = avg_vel_right/timestep_counter;
        if(avg_vel_right.norm() < params.velThreshold){ // not moving
            if(rightGroup.size() == 1){
                category = BimanualLabel::unimanual_left;
                unimanual_left = true;
            }

        }

        Eigen::Vector3f avg_vel_left = Eigen::Vector3f::Zero();
        timestep_counter = 0;

        MMM::BimNode* left;
        for(MMM::BimNode* node: leftGroup){
            if(node->name == "left_hand"){
                left = node;
            }
        }
        for(const auto& timestep : left->velocities){
            if(timestep.first >= t_min && timestep.first < t_max){
                avg_vel_left += timestep.second;
                timestep_counter++;
            }
        }

        avg_vel_left = avg_vel_left/timestep_counter;
        if(avg_vel_left.norm() < params.velThreshold){
            if(unimanual_left){
                category = BimanualLabel::no_motion;
            }else{
                if(leftGroup.size() == 1){
                    category = BimanualLabel::unimanual_right;
                }

            }

        }
        if(category == (+BimanualLabel::undefined)){
            category = BimanualLabel::loosely_coupled;
        }
    }

    //
    // different categories of tight coupling
    // classifies: tightly_coupled_asym_r, tightly_coupled_asym_l, tightly_coupled_sym
    //
    else{
            // get element of hand group interaction list which has contact which belongs to group left_hand
            std::vector<MMM::BimNode*> leftHandgroupInteraction = scene->returnCurrentMembersOfGroup(
                        MMM::BimNode::Group::left_hand, t_min);
            std::vector<MMM::BimNode*> rightHandgroupInteraction = scene->returnCurrentMembersOfGroup(
                        MMM::BimNode::Group::right_hand, t_min);

            MMM::BimNode* mainHandNode;
            std::string otherHandName;
            BimanualLabel mainCategory = BimanualLabel::undefined;
            BimanualLabel otherCategory = BimanualLabel::undefined;


            if (!rightHandgroupInteraction.empty())
            {

                for(const auto& node: rightHandgroupInteraction){
                    if(node->name == "right_hand"){
                        mainHandNode = node;
                    }
                }
                if(mainHandNode == nullptr){
                    throw std::runtime_error("Right hand group interaction list is empty");
                }

                mainCategory = BimanualLabel::tightly_coupled_asym_r;
                otherCategory = BimanualLabel::tightly_coupled_asym_l;
                otherHandName = "left_hand";
            }else{
                throw std::runtime_error("Right hand group interaction list is empty");
            }
            for(MMM::BimNode* node: leftHandgroupInteraction){
                std::string test = node->getGroupIdentityAsString(scene->getNearestTimestep(t_min));
                if(node->getGroupIdentityAsString(scene->getNearestTimestep(t_min)) == otherHandName){
                    // initialize all values
                    Eigen::Vector3f avg_vel_diff = Eigen::Vector3f::Zero();
                    float avg_vel_diff_norm = 0.0;
                    Eigen::Vector3f avg_abs_vel_other = Eigen::Vector3f::Zero();
                    float avg_ang_vel_other = 0;
                    Eigen::Vector3f avg_abs_vel_main = Eigen::Vector3f::Zero();
                    float avg_ang_vel_main = 0;
                    Eigen::Vector3f  hand_offset = mainHandNode->positions.at(t_min) - node->positions.at(t_min);
                    float abs_hand_offset = hand_offset.norm();
                    float diff_hand_offset = 0;
                    int counter = 0;
                    for(const auto& timestep : node->velocities){
                        if(timestep.first >= t_min && timestep.first < t_max){
                            counter++;
                            avg_abs_vel_other += timestep.second.cwiseAbs();
                            avg_ang_vel_other += std::abs(node->angvelocities.at(timestep.first));
                            avg_abs_vel_main += mainHandNode->velocities.at(timestep.first).cwiseAbs();
                            avg_ang_vel_main += std::abs(mainHandNode->angvelocities.at(timestep.first));
                            avg_vel_diff += (timestep.second - mainHandNode->velocities.at(timestep.first)).cwiseAbs();
                            Eigen::Vector3f  current_hand_offset = mainHandNode->positions.at(timestep.first) - node->positions.at(timestep.first);
                            diff_hand_offset += std::abs(current_hand_offset.norm() - abs_hand_offset);
                            avg_vel_diff_norm += std::abs((timestep.second.norm() - mainHandNode->velocities.at(timestep.first).norm()));

                        }
                    }
                    avg_vel_diff = avg_vel_diff/counter;
                    avg_abs_vel_main = avg_abs_vel_main/counter;
                    avg_abs_vel_other = avg_abs_vel_other/counter;

                    diff_hand_offset = diff_hand_offset/counter;
                    avg_ang_vel_other = avg_ang_vel_other/counter;
                    avg_ang_vel_main = avg_ang_vel_main/counter;
                    float ang_factor = 100;
                    avg_vel_diff_norm = avg_vel_diff_norm/counter;
                    if(diff_hand_offset < params.offsetThreshold && scene->gethandGroupInteractionListSizeWithoutPadding(t_min) > 0){
                        category = BimanualLabel::tightly_coupled_sym;
                    }else if((avg_abs_vel_main.norm() + ang_factor*avg_ang_vel_main) < (avg_abs_vel_other.norm() + ang_factor*avg_ang_vel_other)){
                        // determine dominant hand
                        category = otherCategory;
                    }else{
                        category = mainCategory;
                    }
                }
            }
            if (category == (+BimanualLabel::undefined))
            {
                throw std::runtime_error("No category set for " + mainHandNode->name);
            }


        }
        return category;

}

void SlidingClassifier::defragmetSegmentation()
{
    double starttime = 0;
    double endtime = 0;
    // removes segment if smaller than defragment_treshold and segment before and after have the same category
    BimanualLabel prevSegment = BimanualLabel::undefined;
    BimanualLabel currentSegment = BimanualLabel::undefined;
    BimanualLabel nextSegment = BimanualLabel::undefined;
    for(std::size_t i = 1; i < segCategories.size()-1; i++){
        prevSegment = segCategories[i-1].second;
        currentSegment = segCategories[i].second;
        nextSegment = segCategories[i+1].second;
        starttime = segCategories[i].first;
        endtime = segCategories[i+1].first;
        if((endtime-starttime < params.defragmentThreshold)){
            if((+prevSegment) == (+nextSegment)){
                segCategories[i].second = segCategories[i+1].second;
            }
        }

    }
    // removes segment if smaller than defragment_treshold_min and addes it to the next segment
    for(std::size_t i = 1; i < segCategories.size()-1; i++){
        prevSegment = segCategories[i-1].second;
        currentSegment = segCategories[i].second;
        nextSegment = segCategories[i+1].second;
        starttime = segCategories[i].first;
        endtime = segCategories[i+1].first;
        if((endtime-starttime < params.defragmentThresholdMin)){
            segCategories[i].second = segCategories[i+1].second;

        }

    }

}













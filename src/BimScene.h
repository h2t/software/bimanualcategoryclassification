#pragma once
#include "BimParameter.h"
#include "SemanticObjectRelations.h"
#include "BimNode.h"
#include <MMM/Motion/Motion.h>
#include <SemanticObjectRelations/Shapes/shape_containers.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include <SemanticObjectRelations/Spatial/SpatialRelations.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/SceneObject.h>

namespace MMM
{


class BimScene
{
public:
    /**
     * @brief BimScene
     * @param sem: information about semantic object relations
     * @param motions: mmm motion to investigate
     * @param params: struct of parameters needed for full pipeline
     */
    BimScene(SemObjRelations* sem, const MMM::MotionRecordingPtr& motions, BimParameters params);

    //BimScene();


    /**
     * @brief assignGroupIdentities
     * the nodes are asigned to groups for all time steps under consideration of the set background objects
     * @param backgroundelements: objects of type background such as wall, table, etc.
     */
    void assignGroupIdentities(const std::vector<std::string>& backgroundelements);



    /**
     * @brief assignGroupIdentitiesWithoutPadding
     * @param timestep
     */
    void assignGroupIdentitiesWithoutPadding(float timestep);

    /**
     * @brief returnCurrentMembersOfGroup
     * returns all members of fullgrouplist which are of type group at timestep
     * @param fullgrouplist
     * @param group
     * @param timestep
     * @return
     */
    std::vector<MMM::BimNode*> returnCurrentMembersOfGroup(const std::vector<std::unique_ptr<MMM::BimNode>> &fullgrouplist, const MMM::BimNode::Group group, float timestep) const;

    /**
     * @brief returnCurrentMembersOfGroup
     * returns all nodes which are of are of type group at timestep
     * @param group
     * @param timestep
     * @return
     */
    std::vector<MMM::BimNode*> returnCurrentMembersOfGroup(const MMM::BimNode::Group group, float timestep) const;

    /**
     * @brief extractMotionInformation: get the motion information for each node, saves data if BimParameters.motionDataExport is set to true
     * @param motions
     * @param motionname
     */
    void extractMotionInformation(MotionRecording &motions, const std::string& motionname);

    MMM::BimNode* getNodebyName(const std::string& name) const;

    int getHandGroupInteractionListSize(float t) const{
        return getHandGroupInteractionList(t).size();
    }
    int gethandGroupInteractionListSizeWithoutPadding(float timestep) const{
        float t = getNearestTimestep(timestep);
        return nopadding_handGroupInteractionLists.at(t).size();
    }
    std::vector<MMM::BimNode*> getHandGroupInteractionList(float timestep) const{
        float t = getNearestTimestep(timestep);
        return handGroupInteractionLists.at(t);
    }


    /**
     * @brief getNearestTimestep: assignes given timestep to the closest timestep in timestepList
     * @param timestep
     * @return closest timestep in timesteplist
     */
    float getNearestTimestep(float timestep) const{
        auto it = std::lower_bound(timestepList.begin(), timestepList.end(), timestep);
        auto prev = std::prev(it);
        if(std::abs(*it - timestep) < std::abs(timestep - *prev)){
            return *it;
        }
        else{
            return *prev;
        }
        throw std::runtime_error("Timestep out of range");
    }
    /**
     * @brief printCurrentGroupIdentities
     * print group identites of all nodes at timestep
     * @param timestep
     */
    void printCurrentGroupIdentities(float timestep) const;
    /**
     * @brief printHandGroupInteractions
     * print all nodes in handgGroupInteractionList at timestep
     * @param timestep
     */
    void printHandGroupInteractions(float timestep) const;

private:
    /**
     * @brief assignGroupIdentities
     * group identities are set for a single timestep
     * @param timestep
     */
    void assignGroupIdentities(float timestep);
    /**
     * @brief findNeighborsWithRelation
     * @param sem: semantic object relations for motions
     * @param vertex in graph corresponds to node
     * @return vertices which are in contact with quired vertex
     */
    std::vector<semrel::SpatialGraph::Vertex> findNeighborsWithRelation(semrel::SpatialGraph::Vertex vertex) const;

    /// Parameters used in complete category detection pipeline
    BimParameters params;
    /// Input motion
    MMM::MotionRecordingPtr motions;
    /// Semantic object relations for motions
    SemObjRelations* sem;
    /// Including the timesteps of the motion
    std::vector<float> timestepList;
    /// List of all nodes included in the scene
    std::vector<std::unique_ptr<MMM::BimNode>> nodeTypeList;
    /// Contact graphs for each timestep
    std::map<float, semrel::SpatialGraph> graphs;
    std::map<float, semrel::SpatialGraph> nopadding_graphs;
    /// Nodes of the two hand groups which are in contact with each other for each timestep
    std::map<float, std::vector<MMM::BimNode*>> handGroupInteractionLists;
    std::map<float, std::vector<MMM::BimNode*>> nopadding_handGroupInteractionLists;


};



}


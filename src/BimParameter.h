#ifndef BIMPARAMETER_H
#define BIMPARAMETER_H
#include <filesystem>

namespace MMM
{
    /**
     * @brief The BimParameters struct
     * includes all parameters needed fo the bimanual category detection pipeline
     */
    struct BimParameters {
        /// boolean determining whether bounding boxes are used for contact detection
        bool useBoxes = false;
        /// window size in frames for sliding window classifier
        int windowSize = 10;
        /// velocity threshold to detect motion onset
        float velThreshold = 20.0;
        /// threshold for the average difference of the hand offset (to determine symmetrical motions)
        float offsetThreshold = 1.0;
        /// threshold to remove segment if smaller and segment before and after have same type
        float defragmentThreshold = 0.1;
        /// threshold to remove all segements which are smaller
        float defragmentThresholdMin = 0.02;
        /// padding added on top of object to investigate basic contact
        float padding = 8.0;
        /// padding added on top of object to investigate close contact
        float paddingMin = 3.0;
        /// path where .csv with segmentation results are exported to
        std::filesystem::path segmentationResultsPath;
        /// path where motion data for all objects is stored
        std::filesystem::path motionDataPath;
        /// boolean determining if motion data for all objects are saved as csv
        bool motionDataExport = false;


        void checkBimanualCategoryDetectionParameters() const{

            if(windowSize <= 0){
                throw std::runtime_error("[PARAMETER ERROR] Window size has to be positive int");
            }
            if(velThreshold < 0){
                throw std::runtime_error("[PARAMETER ERROR] Velocity threshold size has to be positive");
            }
            if(offsetThreshold < 0){
                throw std::runtime_error("[PARAMETER ERROR] Offset threshold size has to be positive");
            }
            if(defragmentThreshold < defragmentThresholdMin){
                throw std::runtime_error("[PARAMETER ERROR] DefragmentThreshold should be bigger than defragmentThresholdMin");
            }
            if(padding < paddingMin){
                throw std::runtime_error("[PARAMETER ERROR] Padding should be bigger than paddingMin");
            }

            // check if path is valid
            if(!std::filesystem::exists(segmentationResultsPath)){
                throw std::runtime_error("[PARAMETER ERROR] No valid path for segmentation_results_path set");
            }
            if(motionDataExport){
                if(!std::filesystem::exists(motionDataPath)){
                    throw std::runtime_error("[PARAMETER ERROR] No valid path for motion_data_path set");
                }
            }
        }
    };


}
#endif // BIMPARAMETER_H

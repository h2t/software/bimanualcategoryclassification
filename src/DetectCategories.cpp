#include "BimParameter.h"
#include "DetectCategoriesConfiguration.h"
#include "BimNode.h"
#include "BimScene.h"
#include "SlidingClassifier.h"
#include "ExportResults.h"
#include "HandleMotionsWithoutModel.h"
#include "../MMMSemanticObjectRelations/../common/ApplicationBaseConfiguration.h"
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <memory>
#include <string>
#include <vector>
#include <tuple>



using namespace MMM;

int main(int argc, char *argv[]) {
    std::cout << " --- Bimanual Category Detection --- " << std::endl;
    auto* configuration = new MMMBimCategoryDetectionConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        std::cout << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    // Setting parameters
    std::filesystem::path basepath = std::filesystem::current_path();
    std::cout << "BASEPATH IS:   " << basepath << std::endl;
    struct BimParameters params{};
    params.windowSize = 10;
    params.useBoxes = false;  // switch here between using bounding boxes and more precise contacts
    params.motionDataExport = true; // motion data should be exported
    params.velThreshold = 20.0;
    params.offsetThreshold = 1.0;
    params.defragmentThreshold = 0.1;
    params.defragmentThresholdMin = 0.02;
    params.padding = 8.0;
    params.paddingMin = 3.0;
    params.segmentationResultsPath = "../data/Evaluation";
    params.motionDataPath = "../data/MotionData";


    try {
        MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML(true, false));
        motionReader->setHandleMissingModelFile(handleMissingModelFilePath);
        std::filesystem::path motionInputPath = configuration->inputMotionPath;
        std::filesystem::path referenceMotionPath = configuration->inputMotionPath; //motionInputPath.parent_path().parent_path() / "Manual" / motionInputPath.filename();
        MMM::MotionRecordingPtr motions = motionReader->loadMotionRecording(configuration->inputMotionPath);

        // check if parameters are set
        params.checkBimanualCategoryDetectionParameters();

        // Get spatial relations
        std::cout << " --- Get spatial relations --- " << std::endl;
        double timestepDelta = 2.0 / 100.0;
        bool endEffectors = true;
        SemObjRelations sem(motions, timestepDelta, endEffectors, params.useBoxes);

        // Extract Bimanual Scene
        MMM::BimScene bimScene(&sem, motions, params);

        // Extract Motion Information
        std::filesystem::path input(configuration->inputMotionPath);
        bimScene.extractMotionInformation(*motions, input.stem());

        std::vector<std::string> backgroundobjects;
        backgroundobjects.emplace_back("kitchen_sideboard");
        bimScene.assignGroupIdentities(backgroundobjects);
        // Print group identites and nodes in handGroupInteractionList at timestep= 0
        //bimScene->printCurrentGroupIdentities(0);
        //bimScene->printHandGroupInteractions(0);

        // Classification
        std::cout << " --- Get categories --- " << std::endl;
        MMM::SlidingClassifier sliClas(&bimScene, params);
        sliClas.classifyWithSlidingWindow();
        sliClas.getSegmentation(motions);

        // Compare results to reference solution
        MMM::MotionRecordingPtr referencemotion = motionReader->loadMotionRecording(referenceMotionPath);
        exportResults(motions, referencemotion, input.stem(), params);

    }
    catch (std::exception const& e)
    {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }
}






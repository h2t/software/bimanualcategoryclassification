#include "SemanticObjectRelations.h"
#include "../MMMSemanticObjectRelations/SimoxObjectShape.h"

#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

#include <SemanticObjectRelations/Spatial/evaluate_relations.h>
#include <SemanticObjectRelations/ContactDetection.h>
#include <SemanticObjectRelations/SupportAnalysis/GeometricReasoning.h>
#include <SemanticObjectRelations/SupportAnalysis/ActReasoning.h>
#include <SemanticObjectRelations/RelationGraph.h>
#include <SemanticObjectRelations/Hooks/Log.h>
#include <SimoxUtility/math/convert.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/EndEffector/EndEffector.h>
#include <VirtualRobot/Visualization/TriMeshModel.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>
#include <VirtualRobot/CollisionDetection/CollisionChecker.h>
#include <VirtualRobot/CollisionDetection/CollisionCheckerImplementation.h>
#include <VirtualRobot/ManipulationObject.h>


using namespace semrel;
using namespace semrel::spatial;
using namespace MMM;

SemObjRelations::SemObjRelations(const MMM::MotionRecordingPtr& motions, double timeFrequency, bool endEffectors, bool boxShape)
{
    semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::WARNING);

    this->motions = motions;
    this->timestepDelta = timeFrequency;
    std::vector<float> timesteps;
    auto mmm = motions->getReferenceModelMotion();
    if (mmm) timesteps = mmm->getTimesteps();
    else {
        for (const auto& motion : *motions) motion->synchronizeSensorMeasurements(timeFrequency);
        if (motions->size() > 0) timesteps = (*motions->begin())->getTimesteps();
    }

    long id = 0;

    //std::set<std::string> ignore = {"go_pro", "kinect_azure_right", "kinect_azure_left", "kinect_azure_front"};

    for (const MMM::MotionPtr& motion : *motions) {
        if (!motion->getModel()) {
            std::cout << "Ignoring motion " << motion->getName() << " because it has no model" << std::endl;
            continue;
        }
        auto robot = motion->getModel()->cloneScaling();
        robot->reloadVisualizationFromXML();
        if (!motion->getActuatedJointNames().empty()) {
            // add hands to shapes
            if (endEffectors) {
                std::vector<std::pair<std::string, std::string>> endEffectorNames =
                    {std::make_pair("Hand L Root", "left_hand"), std::make_pair("Hand R Root", "right_hand")};
                for (float timestep : timesteps) {
                    motion->initializeModel(robot, timestep);
                    for (unsigned int i = 0; i < endEffectorNames.size(); i++) {
                        auto endEffectorNode = robot->getRobotNode(endEffectorNames[i].first);
                        if (boxShape)
                        {
                            addShape(getBoxShape(endEffectorNode, id + i), timestep);
                        }
                        else
                        {
                            addShape(new SimoxObjectShape(endEffectorNode, id + i, endEffectorNames[i].second, motion->getName()), timestep);
                        }
                        motionNames[id + i] = endEffectorNames[i].second;
                    }
                }
                id += endEffectorNames.size();
            }
        }
        else {
            // add objects to shape
            auto rootNode = robot->getRootNode();
            for (float timestep : timesteps) {
                motion->initializeModel(robot, timestep);
                if (boxShape) addShape(getBoxShape(rootNode, id), timestep);
                else addShape(new SimoxObjectShape(rootNode, id, std::filesystem::path(robot->getFilename()).stem().string(), motion->getName()), timestep);
                motionNames[id] = motion->getName();
            }
            id++;
        }
    }
}


Shape* SemObjRelations::getBoxShape(const VirtualRobot::SceneObjectPtr& node, long id, VirtualRobot::SceneObject::VisualizationType visuType, bool includeChildNodes) const{
    VirtualRobot::VisualizationNodePtr vis = node->getVisualization(visuType);
    if (includeChildNodes) {
        std::vector<VirtualRobot::VisualizationNodePtr> nodes;
        if (vis) nodes.push_back(vis);
        auto children = node->getChildren();
        while (!children.empty()) {
            auto child = *children.rbegin();
            children.pop_back();
            auto childVis = child->getVisualization(visuType);
            if (childVis)
                nodes.push_back(childVis);
            auto childChildren = child->getChildren();
            children.insert(children.begin(), childChildren.begin(), childChildren.end());
        }
        if (nodes.size() == 1)
            vis = nodes.at(0);
        else if (nodes.size() > 1)
            vis = VirtualRobot::VisualizationNode::CreateUnitedVisualization(nodes);
    }

    if (vis) {
        auto boundingBox = vis->getBoundingBox();

        if (false) {
            // Following code computes oobb
            VirtualRobot::TriMeshModelPtr tm = vis->getTriMeshModel();

            if (!tm)
            {
                Eigen::Vector3f minS; Eigen::Vector3f maxS;
                tm->getSize(minS, maxS);
                VirtualRobot::MathTools::OOBB bbox(minS, maxS, vis->getGlobalPose());
            }
        }

        //auto pose = node->getGlobalPose();
        AxisAlignedBoundingBox aabb = AxisAlignedBoundingBox(boundingBox.getMin(), boundingBox.getMax());
        return new Box((aabb.min() + aabb.max()) / 2, Eigen::Matrix3f::Identity(), aabb.extents(), ShapeID(id));
    }
    return nullptr;
}

void SemObjRelations::addShape(Shape* shape, double timestep) {
    if (!shape) return;
    if (shapes.find(timestep) == shapes.end())
    {
        shapes[timestep].clear();
    }
    shapes.at(timestep).emplace_back(shape);
}

SpatialGraph SemObjRelations::evaluateSpatialRelations(double timestep, float distanceEqualityThreshold) const{
    auto it = getIt(timestep);
    if (it != shapes.end()) {
        if (it != shapes.begin()) {
            auto& currentObjects = it->second;
            auto& pastObjects = std::prev(it)->second;
            Parameters params;
            params.distanceEqualityThreshold = distanceEqualityThreshold;
            return evaluateRelations(toShapeMap(currentObjects), toShapeMap(pastObjects), params);
        }
        
            return evaluateStaticRelations(toShapeMap(it->second)); // no dynamic relations available
        
    }
    throw std::runtime_error("Evaluation of spatial relations not possible");

}

std::map<float, SpatialGraph> SemObjRelations::getSpatialGraphsFromContactRelationForInflation(float inflateModelsInMM) const
{
    std::map<float, SpatialGraph> graphs;
    std::vector<float> timesteps;
    auto mmm = motions->getReferenceModelMotion();
    if (mmm) timesteps = mmm->getTimesteps();
    else {
        for (const auto& motion : *motions) motion->synchronizeSensorMeasurements(timestepDelta);
        if (motions->size() > 0) timesteps = (*motions->begin())->getTimesteps();
    }

    std::vector<std::pair<std::string, std::string>> endEffectorNames =
        {std::make_pair("Hand L Palm", "left_hand"), std::make_pair("Hand R Palm", "right_hand")};


    bool firsttimestep = true;
    std::vector<semrel::Shape*> currentObjects;
    long id = 0;

    // Create shapes here once and update object references later
    for (const MMM::MotionPtr& motion : *motions)
    {
        if (!motion->getModel()) {
            MMM_WARNING << "Ignoring motion " << motion->getName() << " because it has no model" << std::endl;
            continue;
        }

        VirtualRobot::RobotPtr robot = motion->getModel();
        robot->reloadVisualizationFromXML();

        for (const VirtualRobot::CollisionModelPtr& colModel : robot->getCollisionModels())
        {
            colModel->inflateModel(inflateModelsInMM);
        }
        if (!motion->getActuatedJointNames().empty()) {

            for (unsigned int i = 0; i < endEffectorNames.size(); i++) {
                auto endEffectorNode = robot->getRobotNode(endEffectorNames[i].first);
                currentObjects.push_back(new SimoxObjectShape(endEffectorNode, id + i, endEffectorNames[i].second, motion->getName()));
                std::cout << int(id + i) << " -> " << endEffectorNames[i].first << std::endl;
            }
            id += endEffectorNames.size();
        }
        else {
            auto rootNode = robot->getRootNode();
            currentObjects.push_back(new SimoxObjectShape(rootNode, id, std::filesystem::path(robot->getFilename()).stem().string(), motion->getName()));
            std::cout << id << " -> " << motion->getName() << std::endl;
            id += 1;
        }
    }


    for(float timestep : timesteps)
    {
        int motionIndex = 0;
        for (const MMM::MotionPtr& motion : *motions)
        {
            if (!motion->getModel()) {
                continue;
            }

            VirtualRobot::RobotPtr robot = motion->getModel();
            ++motionIndex;
            motion->initializeModel(robot, timestep);
        }
        ShapeMap shapeMap = toShapeMap(currentObjects);
        semrel::ContactPointList contactPoints = calculateContactPoints(currentObjects);
        ContactGraph cgraph = semrel::buildContactGraph(contactPoints, shapeMap);


        //after evaluate contact relations
        auto it = getIt(timestep);
        semrel::ShapeList const& objects = it->second;
        shapeMap = toShapeMap(objects);
        SpatialGraph graph;
        for(ContactGraph::Vertex vertex: cgraph.vertices())
        {
            graph.addVertex(*shapeMap.at(vertex.objectID()), {});
        }
        for (SpatialGraph::Vertex vertexFrom : graph.vertices())
        {
            for (SpatialGraph::Vertex vertexTo : graph.vertices())
            {
                ContactGraph::Vertex contactVertexFrom = cgraph.vertex(vertexFrom.attrib().objectID);
                ContactGraph::Vertex contactVertexTo = cgraph.vertex(vertexTo.attrib().objectID);
                SpatialEdge edgeAttribs;
                if (cgraph.hasEdge(contactVertexFrom, contactVertexTo)
                        || cgraph.hasEdge(contactVertexTo, contactVertexFrom))
                {
                    edgeAttribs.relations.set(semrel::SpatialRelations::Type::contact, true);
                }
                // Create bidirectional contact graph
                graph.addEdge(vertexFrom, vertexTo, edgeAttribs);
                graph.addEdge(vertexTo, vertexFrom, edgeAttribs);
            }
        }
        const semrel::SpatialRelations filter({ semrel::SpatialRelations::Type::contact,
                                                semrel::SpatialRelations::Type::dynamic_stable});
        semrel::SpatialGraph contacts_graph = graph.filtered(filter);
        graphs.insert(std::make_pair(timestep, contacts_graph));
        firsttimestep = false;
   }
   return graphs;
}



void addSelfAndAllChildren(VirtualRobot::SceneObjectSetPtr& set, VirtualRobot::SceneObjectPtr const& parent)
{
    set->addSceneObject(parent);
    for (const VirtualRobot::SceneObjectPtr& child : parent->getChildren())
    {
        addSelfAndAllChildren(set, child);
    }
}

semrel::ContactPointList SemObjRelations::calculateContactPoints(std::vector<semrel::Shape*> const& objects) const
{

    semrel::ContactPointList result;

    VirtualRobot::CollisionCheckerPtr collisionChecker = VirtualRobot::CollisionChecker::getGlobalCollisionChecker();
    for (std::size_t i = 0; i < objects.size(); ++i)
    {
        auto const& shapeA = dynamic_cast<SimoxObjectShape const&>(*objects[i]);
        for (std::size_t j = i + 1; j < objects.size(); ++j)
        {
            auto const& shapeB = dynamic_cast<SimoxObjectShape const&>(*objects[j]);
            Eigen::Affine3f poseB(shapeB.object->getGlobalPose());

            auto childrenA = shapeA.object->getChildren();
            auto childrenB = shapeB.object->getChildren();
            if (!childrenA.empty() || !childrenB.empty())
            {
                // A or B has children
                VirtualRobot::SceneObjectSetPtr objectsA(new VirtualRobot::SceneObjectSet);
                addSelfAndAllChildren(objectsA, shapeA.object);

                VirtualRobot::SceneObjectSetPtr objectsB(new VirtualRobot::SceneObjectSet);
                addSelfAndAllChildren(objectsB, shapeB.object);

                if (collisionChecker->checkCollision(objectsA, objectsB))
                {
                    result.emplace_back(&shapeA, &shapeB, Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero());
                }
            }
            else
            {

                VirtualRobot::CollisionModelPtr collModelA = shapeA.object->getCollisionModel();
                VirtualRobot::CollisionModelPtr collModelB = shapeB.object->getCollisionModel();
                // Only find a single contact point to speed up the collision detection
                // Contact point positions are not needed for this analysis

                if (collisionChecker->checkCollision(collModelA, collModelB))
                {
                    result.emplace_back(&shapeA, &shapeB, Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero());
                }
            }
        }
    }

    return result;
}

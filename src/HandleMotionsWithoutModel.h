#pragma once

#include <filesystem>
#include <QSettings>
#include <QFileDialog>
#include <MMM/MMMCore.h>
#include <filesystem>
#include <MMM/Motion/XMLTools.h>
#include "../common/HandleMotionsWithoutModel.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>

void handleMissingModelFilePath(std::filesystem::path &modelFilePath) {
    QSettings settings;
    std::string homedir = getpwuid(getuid())->pw_dir;
    // TODO adapt!
    std::filesystem::path mmmtoolsBuildDir = std::string(MMMTools_BUILD_DIR);
    std::filesystem::path mmmtoolsSourceDir = mmmtoolsBuildDir / "../..";
    std::filesystem::path mmmDefaultPath = mmmtoolsSourceDir / "data/Model/Winter/mmm.xml";
    std::filesystem::path mmmDefaultFilePath(mmmDefaultPath);


    std::filesystem::path cwd = std::filesystem::current_path();

    std::filesystem::path currentProject = std::string(MMMBimCatDetection_BASE_DIR);
    std::string defaultPath = settings.value("model/searchpath", QString::fromStdString(currentProject) + "/../data/Models").toString().toStdString();

    std::string fileName = modelFilePath.stem();
    std::filesystem::path objectDefaultPath = defaultPath + "/" + fileName + "/" + fileName + ".xml";



    if (modelFilePath.generic_string().find("mmm.xml") != std::string::npos && MMM::xml::isValid(mmmDefaultFilePath)) {
        modelFilePath = mmmDefaultPath;
        std::cout << "Loading missing mmm model file from default path '" + mmmDefaultPath.generic_string() + "'" << std::endl;
    } else if (MMM::xml::isValid(objectDefaultPath)) {
        modelFilePath = objectDefaultPath;
        std::cout << "Loading missing object model file from default path '" + objectDefaultPath.generic_string() + "'" << std::endl;
    }else {
        QSettings settings;
        QString storedSearchPath = "model/searchpath/" + QString::fromStdString(fileName);
        if (settings.contains(storedSearchPath)) {
            std::string searchPath = settings.value(storedSearchPath).toString().toStdString();
            if (MMM::xml::isValid(searchPath)) {
                modelFilePath = searchPath;
                return;
            }
        }
        modelFilePath = QFileDialog::getOpenFileName(0, QString::fromStdString("Model file could not be loaded from '" + modelFilePath.generic_string() + "'. Choose a matching model file in your file system!"), QString::fromStdString(defaultPath), QString::fromStdString("model files (*.xml)")).toStdString();
        if (!modelFilePath.empty()) {
            settings.setValue("model/searchpath", QString::fromStdString(modelFilePath.parent_path().parent_path()));
            settings.setValue(storedSearchPath, QString::fromStdString(modelFilePath));
        }
    }
}

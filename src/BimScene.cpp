#include "BimScene.h"
#include "BimNode.h"
#include "BimScene.h"

#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

#include <SemanticObjectRelations/Spatial/evaluate_relations.h>
#include <SimoxUtility/math/convert.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/EndEffector/EndEffector.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>
#include <utility>
#include <memory>

using namespace semrel;
using namespace semrel::spatial;
using namespace MMM;

/**
 * @brief check if node is contained in list
 * @param nodeList
 * @return true if node in nodeList else false
 */
bool isNodeInNodeListTest(BimNode* node, std::vector<MMM::BimNode*> &nodeList){

    return !(std::find(nodeList.begin(), nodeList.end(), node) == nodeList.end());
}

BimScene::BimScene(SemObjRelations* semRel, const MotionRecordingPtr& motions, BimParameters params)
{
    this->motions = motions;
    this->sem = std::move(semRel);
    //this->boundingBoxes = params.useBoxes;
    this->params = params;

    //float maxTimestep = motions->getFirstMotion()->getMaxTimestep();
    timestepList = motions->getReferenceModelMotion()->getTimesteps();
    /*std::cout << timestepList.at(0) << "  " << timestepList.at(timestepList.size()-1) << std::endl;
    for(const MMM::MotionPtr& motion : *motions){
        if(motion->getMaxTimestep() > maxTimestep){
            maxTimestep = motion->getMaxTimestep();
        }
    }*/

    // Creating contact graphs
    if(params.useBoxes){
        for(float timestep : timestepList){
            semrel::SpatialGraph graph;
            graph = sem->evaluateSpatialRelations(timestep, 1);
            const semrel::SpatialRelations filter({ semrel::SpatialRelations::Type::contact,
                                                    semrel::SpatialRelations::Type::dynamic_stable});
            semrel::SpatialGraph contacts_graph = graph.filtered(filter);
            graphs.insert(std::make_pair(timestep, contacts_graph));
        }

    }else{
        graphs = sem->getSpatialGraphsFromContactRelationForInflation(params.padding);
        nopadding_graphs = sem->getSpatialGraphsFromContactRelationForInflation(params.paddingMin);

    }

    //all nodes need to be present at the beginning
    for(semrel::SpatialGraph::Vertex m: graphs.begin()->second.vertices()){
        nodeTypeList.push_back(std::make_unique<MMM::BimNode>(sem->getMotionName(m.attrib().objectID)));
    }
}



void BimScene::assignGroupIdentities(const std::vector<std::string>& backgroundelements)
{
    /*float maxTimestep = motions->getFirstMotion()->getMaxTimestep();
    for(const MMM::MotionPtr& motion : *motions){
        if(motion->getMaxTimestep() > maxTimestep){
            maxTimestep = motion->getMaxTimestep();
        }
    }*/
    for(const std::string& name: backgroundelements){
        for(float timestep : timestepList){
            nodeTypeList.at(sem->getShapeID(name))->groupIdentities[timestep] = MMM::BimNode::Group::background;
        }
    }
    for(float timestep : timestepList){
        assignGroupIdentitiesWithoutPadding(timestep);
    }
    for(float timestep : timestepList){
        assignGroupIdentities(timestep);
    }

}

void BimScene::assignGroupIdentities(float timestep)
{
    for(const std::unique_ptr<MMM::BimNode> &node: nodeTypeList){
        if(node->groupIdentities[timestep] != MMM::BimNode::Group::background){
            node->groupIdentities[timestep] = MMM::BimNode::Group::undefined;
        }
    }
    getNodebyName("left_hand")->groupIdentities[timestep] = MMM::BimNode::Group::left_hand;
    getNodebyName("right_hand")->groupIdentities[timestep] = MMM::BimNode::Group::right_hand;


    // check if hand groups of previous state are still true
    float previous;
    if(timestep != timestepList.at(0)){


        for(float t : timestepList){
            if(t == timestep){
                break;
            }
            previous = t;
        }
        std::vector<MMM::BimNode*> rightListOld = returnCurrentMembersOfGroup(nodeTypeList, MMM::BimNode::Group::right_hand, previous);
        std::vector<MMM::BimNode*> leftListOld = returnCurrentMembersOfGroup(nodeTypeList, MMM::BimNode::Group::left_hand, previous);


        auto& graph = graphs.at(timestep);
        if (graph.numVertices() == 0)
        {
            std::cout << "Graph has no vertices for timestep: " << timestep;
            throw std::runtime_error("");
        }

        // check if hand groups of previous state are still true
        ShapeID shapeID(sem->getShapeID("right_hand"));
        if (!graph.hasVertex(shapeID))
        {
            std::cout << "Graph at timestap " << timestep << " does not have vertex "
                      << shapeID << " with name " << "right_hand";
            throw std::runtime_error("");
        }

        auto member_vertex = graph.vertex(shapeID);
        std::vector<semrel::SpatialGraph::Vertex> vertexNeighborListRight = findNeighborsWithRelation(member_vertex);

        for(const MMM::BimNode* element: rightListOld){
            if(element->name != "right_hand"){
                for(semrel::SpatialGraph::Vertex neighborVertex : vertexNeighborListRight){
                    if(nodeTypeList.at(neighborVertex.attrib().objectID).get() == element){
                        getNodebyName(element->name)->groupIdentities[timestep] = MMM::BimNode::Group::right_hand;
                    }
                }
            }
        }

        // check if hand groups of previous state are still true
        ShapeID shapeID_left(sem->getShapeID("left_hand"));
        if (!graph.hasVertex(shapeID_left))
        {
            std::cout << "Graph at timestap " << timestep << " does not have vertex "
                      << shapeID_left << " with name " << "right_hand";
            throw std::runtime_error("");
        }

        auto member_vertex_left = graph.vertex(shapeID);
        std::vector<semrel::SpatialGraph::Vertex> vertexNeighborListLeft = findNeighborsWithRelation(member_vertex_left);

        for(MMM::BimNode* element: leftListOld){
            if(element->name != "left_hand"){
                for(semrel::SpatialGraph::Vertex neighborVertex : vertexNeighborListRight){
                    if(nodeTypeList.at(neighborVertex.attrib().objectID).get() == element){
                        getNodebyName(element->name)->groupIdentities[timestep] = MMM::BimNode::Group::left_hand;
                    }
                }
            }
        }


    }


    auto& handGroupInteractionList = handGroupInteractionLists[timestep];
    handGroupInteractionList.clear();
    bool addedNewVertex = true;
    while(addedNewVertex){
        addedNewVertex = false;
        std::vector<MMM::BimNode*> rightList = returnCurrentMembersOfGroup(nodeTypeList, MMM::BimNode::Group::right_hand, timestep);
        std::vector<MMM::BimNode*> leftList = returnCurrentMembersOfGroup(nodeTypeList, MMM::BimNode::Group::left_hand, timestep);
        for(MMM::BimNode const* member: leftList)
        {
            auto& graph = graphs.at(timestep);
            if (graph.numVertices() == 0)
            {
                std::cout << "Graph has no vertices for timestep: " << timestep;
                throw std::runtime_error("");
            }

            ShapeID shapeID(sem->getShapeID(member->name));
            if (!graph.hasVertex(shapeID))
            {
                std::cout << "Graph at timestap " << timestep << " does not have vertex "
                          << shapeID << " with name " << member->name;
                throw std::runtime_error("");
            }

            auto member_vertex = graph.vertex(shapeID);
            std::vector<semrel::SpatialGraph::Vertex> vertexNeighborListRight = findNeighborsWithRelation(member_vertex);
            for(auto vertex: vertexNeighborListRight){
                BimNode::Group& group = nodeTypeList.at(vertex.attrib().objectID)->groupIdentities[timestep];
                if (group == MMM::BimNode::Group::undefined){
                    group = MMM::BimNode::Group::left_hand;
                    addedNewVertex = true;
                }else if(group == MMM::BimNode::Group::right_hand){
                    if(!isNodeInNodeListTest(nodeTypeList.at(vertex.attrib().objectID).get(), handGroupInteractionList)){
                        handGroupInteractionList.push_back(nodeTypeList.at(vertex.attrib().objectID).get());
                    }
                }
            }
        }
        for(const MMM::BimNode* member: rightList){
            ShapeID shapeID(sem->getShapeID(member->name));
            auto member_vertex = graphs[timestep].vertex(shapeID);
            std::vector<semrel::SpatialGraph::Vertex> vertexNeighborListLeft = findNeighborsWithRelation(member_vertex);
            for(auto vertex: vertexNeighborListLeft){
                BimNode::Group& group = nodeTypeList.at(vertex.attrib().objectID)->groupIdentities[timestep];
                if (group == MMM::BimNode::Group::undefined){
                    group = MMM::BimNode::Group::right_hand;
                    addedNewVertex = true;
                }else if(group == MMM::BimNode::Group::left_hand){
                    if(!isNodeInNodeListTest(nodeTypeList.at(vertex.attrib().objectID).get(), handGroupInteractionList)){
                        handGroupInteractionList.push_back(nodeTypeList.at(vertex.attrib().objectID).get());
                    }
                }
            }
        }


    }
    for(const auto& m: nodeTypeList){
        if (m->groupIdentities[timestep] == MMM::BimNode::Group::undefined){
            m->groupIdentities[timestep] = MMM::BimNode::Group::scene;
        }
    }
}

void BimScene::printCurrentGroupIdentities(float t) const
{
    // show current group identities
    t = getNearestTimestep(t);
    for(const auto& m: nodeTypeList){
        std::cout << m->name << " has group identity: " << m->getGroupIdentityAsString(t) << std::endl;//<< std::string(m->groupidentity);
    }


}

void BimScene::printHandGroupInteractions(float t) const
{
    t = getNearestTimestep(t);
    for(MMM::BimNode* m: handGroupInteractionLists.at(t)){
        std::cout << m->name << " has group identity: " << m->getGroupIdentityAsString(t) << std::endl;
    }
}

std::vector<BimNode*> BimScene::returnCurrentMembersOfGroup(std::vector<std::unique_ptr<MMM::BimNode>> const& fullgrouplist, BimNode::Group const group, float timestep) const
{
    std::vector<MMM::BimNode*> outputList;
    for(const auto& node: fullgrouplist){
        if(node->groupIdentities[getNearestTimestep(timestep)] == group){
            outputList.push_back(node.get());
        }
    }
    return outputList;
}

void BimScene::extractMotionInformation(MMM::MotionRecording& motions, const std::string& motionname)
{
    for(auto const& motion : motions){
        MMM::BimNode* currentNode;
        for(float t : timestepList){
            VirtualRobot::RobotPtr robot = motion->getModel();
            if (motion->hasSensor(ModelPoseSensor::TYPE) && motion->hasSensor(KinematicSensor::TYPE)) {
                // look for node to human
                motion->initializeModel(robot, t); // updates robot
                currentNode = getNodebyName("left_hand");
                currentNode->positions.insert(std::make_pair(t, robot->getRobotNode("Hand L TCP")->getGlobalPosition()));
                currentNode->orientations.insert(std::make_pair(t, robot->getRobotNode("Hand L TCP")->getGlobalOrientation()));
                currentNode = getNodebyName("right_hand");
                currentNode->positions.insert(std::make_pair(t, robot->getRobotNode("Hand R TCP")->getGlobalPosition()));
                currentNode->orientations.insert(std::make_pair(t, robot->getRobotNode("Hand R TCP")->getGlobalOrientation()));
            }else{
                currentNode = getNodebyName(motion->getName());
                currentNode->positions.insert(std::make_pair(t, motion->getRootPose(t).block<3,1>(0,3)));
                currentNode->orientations.insert(std::make_pair(t, motion->getRootPose(t).block<3,3>(0,0)));
            }
        }
        for(const auto& node: nodeTypeList){
            node->calcVelocityData();
            if(this->params.motionDataExport){
                node->exportMotionDataCSV(motionname, params);
            }
        }
    }
}

BimNode* BimScene::getNodebyName(const std::string& name) const
{
    for (auto& member: nodeTypeList){
        if (member->name == name){
            return member.get();
        }

    }
    throw std::runtime_error("No with name " + name + " was found.");
}


std::vector<BimNode *> BimScene::returnCurrentMembersOfGroup(BimNode::Group const group, float timestep) const
{
    return returnCurrentMembersOfGroup(this->nodeTypeList, group, timestep);
}

std::vector<SpatialGraph::Vertex> BimScene::findNeighborsWithRelation(SpatialGraph::Vertex vertex) const
{
    // Finds neighbors with certain relation
    auto outEdges = vertex.outEdges();
    auto& attrib = vertex.attrib();
    std::string name = sem->getMotionName(attrib.objectID);
    std::vector<semrel::SpatialGraph::Vertex> vertexNeighborList;
    for (semrel::SpatialGraph::Edge edge : outEdges)
    {
        if (edge.attrib().relations.get(semrel::SpatialRelations::Type::contact))
        {
            semrel::SpatialGraph::Vertex target = edge.target();
            vertexNeighborList.push_back(target);
        }
    }
    if(vertexNeighborList.empty()){
        std::cout << "No neighbors with this relation" << std::endl;
    }
    return vertexNeighborList;
}

void BimScene::assignGroupIdentitiesWithoutPadding(float timestep)
{
    for(const auto& node: nodeTypeList){
        if(node->groupIdentities[timestep] != MMM::BimNode::Group::background){
            node->groupIdentities[timestep] = MMM::BimNode::Group::undefined;
        }
    }
    getNodebyName("left_hand")->groupIdentities[timestep] = MMM::BimNode::Group::left_hand;
    getNodebyName("right_hand")->groupIdentities[timestep] = MMM::BimNode::Group::right_hand;


    // check if hand groups of previous state are still true
    float previous;
    if(timestep != timestepList.at(0)){


        for(float t : timestepList){
            if(t == timestep){
                break;
            }
            previous = t;
        }
        std::vector<MMM::BimNode*> rightListOld = returnCurrentMembersOfGroup(nodeTypeList, MMM::BimNode::Group::right_hand, previous);
        std::vector<MMM::BimNode*> leftListOld = returnCurrentMembersOfGroup(nodeTypeList, MMM::BimNode::Group::left_hand, previous);


        auto& graph = nopadding_graphs.at(timestep);
        if (graph.numVertices() == 0)
        {
            std::cout << "Graph has no vertices for timestep: " << timestep;
            throw std::runtime_error("");
        }

        // check if hand groups of previous state are still true
        ShapeID shapeID(sem->getShapeID("right_hand"));
        if (!graph.hasVertex(shapeID))
        {
            std::cout << "Graph at timestap " << timestep << " does not have vertex "
                      << shapeID << " with name " << "right_hand";
            throw std::runtime_error("");
        }

        auto member_vertex = graph.vertex(shapeID);
        std::vector<semrel::SpatialGraph::Vertex> vertexNeighborListRight = findNeighborsWithRelation(member_vertex);

        for(const MMM::BimNode* element: rightListOld){
            if(element->name != "right_hand"){
                for(semrel::SpatialGraph::Vertex neighborVertex : vertexNeighborListRight){
                    if(nodeTypeList.at(neighborVertex.attrib().objectID).get() == element){
                        getNodebyName(element->name)->groupIdentities[timestep] = MMM::BimNode::Group::right_hand;
                    }
                }
            }
        }

        // check if hand groups of previous state are still true
        ShapeID shapeID_left(sem->getShapeID("left_hand"));
        if (!graph.hasVertex(shapeID_left))
        {
            std::cout << "Graph at timestap " << timestep << " does not have vertex "
                      << shapeID_left << " with name " << "right_hand";
            throw std::runtime_error("");
        }

        auto member_vertex_left = graph.vertex(shapeID);
        std::vector<semrel::SpatialGraph::Vertex> vertexNeighborListLeft = findNeighborsWithRelation(member_vertex_left);

        for(const MMM::BimNode* element: leftListOld){
            if(element->name != "left_hand"){
                for(semrel::SpatialGraph::Vertex neighborVertex : vertexNeighborListRight){
                    if(nodeTypeList.at(neighborVertex.attrib().objectID).get() == element){
                        getNodebyName(element->name)->groupIdentities[timestep] = MMM::BimNode::Group::left_hand;
                    }
                }
            }
        }


    }


    auto& handGroupInteractionList = nopadding_handGroupInteractionLists[timestep];
    handGroupInteractionList.clear();
    bool addedNewVertex = true;
    while(addedNewVertex){
        addedNewVertex = false;
        std::vector<MMM::BimNode*> rightList = returnCurrentMembersOfGroup(nodeTypeList, MMM::BimNode::Group::right_hand, timestep);
        std::vector<MMM::BimNode*> leftList = returnCurrentMembersOfGroup(nodeTypeList, MMM::BimNode::Group::left_hand, timestep);
        for(MMM::BimNode const* member: leftList)
        {
            auto& graph = nopadding_graphs.at(timestep);
            if (graph.numVertices() == 0)
            {
                std::cout << "Graph has no vertices for timestep: " << timestep;
                throw std::runtime_error("");
            }

            ShapeID shapeID(sem->getShapeID(member->name));
            if (!graph.hasVertex(shapeID))
            {
                std::cout << "Graph at timestap " << timestep << " does not have vertex "
                          << shapeID << " with name " << member->name;
                throw std::runtime_error("");
            }

            auto member_vertex = graph.vertex(shapeID);
            std::vector<semrel::SpatialGraph::Vertex> vertexNeighborListRight = findNeighborsWithRelation(member_vertex);
            for(auto vertex: vertexNeighborListRight){
                BimNode::Group& group = nodeTypeList.at(vertex.attrib().objectID)->groupIdentities[timestep];
                if (group == MMM::BimNode::Group::undefined){
                    group = MMM::BimNode::Group::left_hand;
                    addedNewVertex = true;
                }else if(group == MMM::BimNode::Group::right_hand){
                    if(!isNodeInNodeListTest(nodeTypeList.at(vertex.attrib().objectID).get(), handGroupInteractionList)){
                        handGroupInteractionList.push_back(nodeTypeList.at(vertex.attrib().objectID).get());
                    }
                }
            }
        }
        for(const MMM::BimNode* member: rightList){
            ShapeID shapeID(sem->getShapeID(member->name));
            auto member_vertex = nopadding_graphs[timestep].vertex(shapeID);
            std::vector<semrel::SpatialGraph::Vertex> vertexNeighborListLeft = findNeighborsWithRelation(member_vertex);
            for(auto vertex: vertexNeighborListLeft){
                BimNode::Group& group = nodeTypeList.at(vertex.attrib().objectID)->groupIdentities[timestep];
                if (group == MMM::BimNode::Group::undefined){
                    group = MMM::BimNode::Group::right_hand;
                    addedNewVertex = true;
                }else if(group == MMM::BimNode::Group::left_hand){
                    if(!isNodeInNodeListTest(nodeTypeList.at(vertex.attrib().objectID).get(), handGroupInteractionList)){
                        handGroupInteractionList.push_back(nodeTypeList.at(vertex.attrib().objectID).get());
                    }
                }
            }
        }
    }

    for(const auto& m: nodeTypeList){
        if (m->groupIdentities[timestep] == MMM::BimNode::Group::undefined){
            m->groupIdentities[timestep] = MMM::BimNode::Group::scene;
        }
    }
}







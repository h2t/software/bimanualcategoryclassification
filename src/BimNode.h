#pragma once

#include <MMM/Motion/Motion.h>
#include <SemanticObjectRelations/Shapes/shape_containers.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/SceneObject.h>
#include "BimParameter.h"


namespace MMM
{

class BimNode
{
public:
    /**
     * @brief BimNodeType
     * @param name
     */
    BimNode(std::string name);

    /**
     * @brief The Group enum
     */
    enum class Group : int
    {
        /// Left hand and objects in contact with it
        left_hand = 0,
        /// Right hand and objects in contact with it
        right_hand = 1,
        /// Objects in the scene which are fixed and can not be manipulated e.g. wall, table. These have to be set manually.
        background = 2,
        /// Objects which are not defined as background or in contact with the hands
        scene = 3,
        /// Undefined group identity
        undefined = 4
    };

    /**
     * @brief calcVelocityData
     * calculate translational and angular velocity data based on smoothed position data
     */
    void calcVelocityData();

    /**
     * @brief exportMotionDataCSV
     * @param motionname: defines name of directory where motion data is exported to
     */
    void exportMotionDataCSV(const std::string& motionname, const BimParameters params) const;



    /**
     * @brief getGroupidentityAsString
     * @param t timestep
     * @return name of the group identity of the node at time t
     */
    std::string getGroupIdentityAsString(double t) const;


    /// unique node name
    std::string name;
    /// group identity of node for each timestep
    std::map<double, Group> groupIdentities;
    /// motion data for each timestep
    std::map<double, Eigen::Vector3f> positions;
    std::map<double, Eigen::Matrix3f> orientations;
    std::map<double, Eigen::Vector3f> velocities;
    std::map<double, float> angvelocities;


private:


};


}


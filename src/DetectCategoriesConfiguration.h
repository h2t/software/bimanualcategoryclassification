#pragma once
#include "BimNode.h"
#include "BimParameter.h"
#include "../MMMSemanticObjectRelations/SemanticObjectRelations.h"
#include "../MMMSemanticObjectRelations/../common/ApplicationBaseConfiguration.h"


#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

#include <MMM/MMMCore.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include <SemanticObjectRelations/Spatial/SpatialRelations.h>
#include <VirtualRobot/RuntimeEnvironment.h>
#include <filesystem>
#include <string>

/**
 * @brief The MMMBimCategoryDetectionConfiguration class
 * Configuration
 */
class MMMBimCategoryDetectionConfiguration : public ApplicationBaseConfiguration
{

public:
    std::string inputMotionPath;
    std::string inputMotionDir;
    std::vector<std::filesystem::path> sensorPluginPaths;
    std::string outputDir;

    MMMBimCategoryDetectionConfiguration() : ApplicationBaseConfiguration()
    {
    }


    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion", "mmm motion file path");
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotionDir", "mmm motion file directory");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");
        VirtualRobot::RuntimeEnvironment::considerKey("outputDir");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPath = getParameter("inputMotion", false, true);
        inputMotionDir = getParameter("inputMotionDir", false, false);
        if (inputMotionPath.empty() && inputMotionDir.empty())
            valid = false;
        sensorPluginPaths = getPaths("sensorPlugins");
        outputDir = getParameter("outputDir", false, false);
        if (outputDir.empty())
            outputDir = inputMotionPath.empty() ? inputMotionDir : std::filesystem::path(inputMotionPath).parent_path().string();

        return valid;
    }

    void print()
    {
        std::cout << "*** Bimanual Category Detection ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;
        std::cout << "Output motion: " << outputDir << std::endl;
    }
};


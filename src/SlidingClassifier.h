#pragma once
#include "BimParameter.h"
#include "BimScene.h"
#include "BimNode.h"
#include "../MMMSemanticObjectRelations/SemanticObjectRelations.h"
#include <MMM/Motion/Annotation/BimanualLabel/BimanualAnnotation.h>
#include <MMM/Motion/Motion.h>
#include <SemanticObjectRelations/Shapes/shape_containers.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include <SemanticObjectRelations/Spatial/SpatialRelations.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/SceneObject.h>


namespace MMM
{


class SlidingClassifier
{
public:
    /**
     * @brief SlidingClassifier
     * @param scene: including scene graph for each time step
     * @param params: struct including window size and defragmentation thresholds
     */
    SlidingClassifier(MMM::BimScene* scene, BimParameters params);
    SlidingClassifier(const SlidingClassifier&) = delete;
    SlidingClassifier& operator=(const SlidingClassifier&) = delete;
    ~SlidingClassifier() {
    }

    /**
     * @brief classifyWithSlidingWindow
     * executes the classification using the parameters set in constructor
     */
    void classifyWithSlidingWindow();

    /**
     * @brief defragmetSegmentation
     * executes defragmentation on previously computed segmentation
     */
    void defragmetSegmentation();



    void getSegmentation(const MMM::MotionRecordingPtr& motions);



private:
    MMM::BimanualLabel classifyWindow(const MMM::BimScene* scene, float t_min, float t_max) const;
    /// Struct including parameters for complete pipeline
    BimParameters params{};
    /// Scene graph for each timestep of the motion
    MMM::BimScene* scene;
    /// Categories for each timestep
    std::vector<BimanualLabel> categories;
    /// including the start timestep and label of each segment
    std::vector<std::pair<double, BimanualLabel>> segCategories;



};

}


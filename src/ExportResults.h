#ifndef EXPORTRESULTS_H
#define EXPORTRESULTS_H
#include "BimParameter.h"
#include <MMM/Motion/Segmentation/AbstractMotionSegment.h>
#include <MMM/Motion/Segmentation/MotionSegment.h>
#include <MMM/Motion/Segmentation/Segmentation.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Segmentation/MotionRecordingSegment.h>
#include <MMM/Motion/Annotation/ActionLabel/ActionLabelAnnotation.h>
#include <MMM/Motion/Annotation/BimanualLabel/BimanualAnnotation.h>


using namespace MMM;

void exportResults(const MMM::MotionRecordingPtr& motions, const MMM::MotionRecordingPtr& referenceMotions, const std::string& motionname, const BimParameters params)
{
    MMM::MotionRecordingSegmentPtr mosegment = motions->getSegment();
    MMM::MotionSegmentationList mosegmentationList = mosegment->getSegmentations();
    std::map<float, MotionSegmentPtr> currentSegmentMap;
    for(const MotionSegmentationPtr& segmentation: mosegmentationList){
        auto annList = segmentation->getLastSegment()->getDerivedAnnotations<BimanualAnnotation>();
        if(!annList.empty()){
            // process only bimanual segmentation
           currentSegmentMap = segmentation->getSegments();
        }else{
            throw std::runtime_error(mosegment->getName() + "does not include bimanual segmentation");
        }
    }

    // get manually labelled reference segmentation
    MMM::MotionRecordingSegmentPtr mosegment_ref = referenceMotions->getSegment();
    MMM::MotionSegmentationList mosegmentationList_ref = mosegment_ref->getSegmentations();
    std::map<float, MotionSegmentPtr> currentSegmentMap_ref;
    bool gotSegmentation = false;
    for(const MotionSegmentationPtr& segmentation: mosegmentationList_ref){
        std::string test = segmentation->getType(); // Here always manual
        if(segmentation->getType() == "BimanualAnnotation"){
            std::cout << "TRUE" << std::endl;
        }
        auto annList = segmentation->getLastSegment()->getDerivedAnnotations<BimanualAnnotation>();
        if(!annList.empty()){
           // process only bimanual segmentation
           currentSegmentMap_ref = segmentation->getSegments();
           /*for(const std::pair<float, MotionSegmentPtr>& segment: currentSegmentMap_ref){
               auto annotationList = segment.second->getDerivedAnnotations<BimanualAnnotation>();
           }*/
           gotSegmentation = true;
       }

    }
    if(!gotSegmentation){
        throw std::runtime_error("Reference segmentation for " + mosegment->getName() + "is not valid");
    }

    // export long lists for python analysis
    std::vector<float> timestepList = motions->getReferenceModelMotion()->getTimesteps();
    std::filesystem::path basepath = std::filesystem::current_path();
    std::filesystem::current_path(params.segmentationResultsPath);
    std::filesystem::create_directory(motionname);
    std::filesystem::path motionpath = params.segmentationResultsPath / motionname;
    std::filesystem::current_path(basepath);

    // export framewise result of rule-based classification
    std::filesystem::path posFileName = motionpath / "prediction.csv";
    std::ofstream posFile (posFileName.string());
    int timestep = 0;
    std::string label;
    bool first = true;
    for(const auto& cat : currentSegmentMap_ref){
        if(first){
            label = cat.second->getDerivedAnnotations<BimanualAnnotation>().at(0)->getLabel()._to_string();
            first = false;
        }else{
            while(timestepList.at(timestep) < cat.first && timestep < int(timestepList.size())-params.windowSize-1){
                posFile << label << std::endl;
                timestep++;
            }
            label = cat.second->getDerivedAnnotations<BimanualAnnotation>().at(0)->getLabel()._to_string();
        }
    }
    while(timestep < int(timestepList.size())-params.windowSize-1){
        posFile << label << std::endl;
        timestep++;
    }
    posFile.close();

    // export framewise result of reference solution
    std::filesystem::path posFileName2 = motionpath /  "true.csv";
    std::ofstream posFile2 (posFileName2.string());
    int timestep2 = 0;
    std::string label2;
    bool first2 = true;
    for(const auto& cat : currentSegmentMap){
        if(first2){
            label2 = cat.second->getDerivedAnnotations<BimanualAnnotation>().at(0)->getLabel()._to_string();
            first2 = false;
        }else{
            while(timestepList.at(timestep2) < cat.first){
                posFile2 << label2 << std::endl;
                timestep2++;
            }
            label2 = cat.second->getDerivedAnnotations<BimanualAnnotation>().at(0)->getLabel()._to_string();
        }
    }
    while(timestep2 < int(timestepList.size())-params.windowSize-1){
        posFile2 << label2 << std::endl;
        timestep2++;
    }
    posFile2.close();
}

#endif // EXPORTRESULTS_H

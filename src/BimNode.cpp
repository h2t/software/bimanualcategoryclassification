#include "BimNode.h"

#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

#include <SemanticObjectRelations/Spatial/evaluate_relations.h>
#include <SimoxUtility/math/convert.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/EndEffector/EndEffector.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>
 #include <utility> 

using namespace semrel;
using namespace semrel::spatial;
using namespace MMM;


BimNode::BimNode(std::string name)
{
    this->name = std::move(name);
}

std::string BimNode::getGroupIdentityAsString(double t) const
{
    switch(this->groupIdentities.at(t)){
        case Group::left_hand: return "left_hand";
        case Group::right_hand: return "right_hand";
        case Group::background: return "background";
        case Group::scene: return "scene";
        case Group::undefined: return "undefined";
    }
    return "Error";

}

void BimNode::calcVelocityData()
{
    std::vector<Eigen::Vector3f> pos;
    std::vector<Eigen::Matrix3f> ori;
    std::vector<float> timesteps;
    for (auto positionelement : positions){
        pos.push_back(positionelement.second);
        timesteps.push_back(positionelement.first);
    }
    for (const auto& orielement: orientations){
        ori.push_back(orielement.second);
    }

    // smooth position data with window_size 5
    std::vector<Eigen::Vector3f> means;
    int size = pos.size();
    int window_size = 5;
    for (int i = 0; i < size; ++i)
    {
        int last = i + window_size;
        last = std::min(last, size - 1);

        Eigen::Vector3f sum = Eigen::Vector3f::Zero();
        int counter = 0;
        for (int j = i; j < last; ++j)
        {
            sum += pos[j];
            counter++;
        }
        Eigen::Vector3f mean = sum / counter;

        means.push_back(mean);
    }

    // calculate translational and angular velocities
    for (int i = 0; i < int(means.size()); ++i)
    {
        int bottom = std::max(0, i - 1);
        int top = std::min(size - 1, i + 1);
        // angular velocity
        Eigen::Matrix3f rotdiff = ori[bottom].inverse()*ori[top];
        Eigen::AngleAxisf newAngleAxis(rotdiff);
        this->angvelocities.insert(std::make_pair(timesteps[i], float(newAngleAxis.angle())));
        // translational velocity
        Eigen::Vector3f posdiff = means[top] - means[bottom];
        float timediff = timesteps[top] - timesteps[bottom];
        this->velocities.insert(std::make_pair(timesteps[i], posdiff/timediff));
    }
}

void BimNode::exportMotionDataCSV(const std::string& motionname, const BimParameters params) const
{
    std::filesystem::path basepath = std::filesystem::current_path();
    std::filesystem::current_path(params.motionDataPath);
    std::filesystem::create_directory(motionname);
    std::filesystem::path motionpath = params.motionDataPath / motionname;
    std::filesystem::current_path(basepath);
    // export data for all objects not listed here
    if(this->name != "kinect_azure_right" && this->name != "kinect_azure_front" && this->name != "kinect_azure_left" && this->name != "go_pro" && this->name != "kitchen_sideboard"){
        std::filesystem::path posFileName = motionpath/ (this->name + "_pos.csv");
        std::ofstream posFile (posFileName.string());
        for(auto pos : positions){
            posFile << pos.first << "," << pos.second.x() << "," << pos.second.y() << "," << pos.second.z() << std::endl;
        }
        posFile.close();

        std::string velFileName = motionpath / (this->name + "_vel.csv");
        std::ofstream velFile (velFileName);
        for(auto vel : velocities){
            velFile << vel.first << "," << vel.second.x() << "," << vel.second.y() << "," << vel.second.z() << std::endl;
        }
         velFile.close();
    }
}







#pragma once

#include <MMM/Motion/Motion.h>
#include <SemanticObjectRelations/Shapes/shape_containers.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include <SemanticObjectRelations/ContactDetection/ContactGraph.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/SceneObject.h>

namespace MMM
{

class SemObjRelations
{
public:
    /**
     * @brief SemObjRelations: adding all objects and the hands to shapes
     * @param motions
     * @param timeFrequency
     * @param endEffectors
     * @param boxShape
     */
    SemObjRelations(const MMM::MotionRecordingPtr& motions, double timestepDelta = 0.01, bool endEffectors = true, bool boxShape = true);

    /**
     * @brief evaluateSpatialRelations
     * @param timestep
     * @param distanceEqualityThreshold: objects within this distance are considered in contact
     * @return contact graph
     */
    semrel::SpatialGraph evaluateSpatialRelations(double timestep, float distanceEqualityThreshold = 30) const;

    /**
     * @brief getSpatialGraphsFromContactRelationForInflation
     * @param inflateModelsInMM: how much models are inflated for contact detection
     * @return contact graphs for each timestep
     */
    std::map<float, semrel::SpatialGraph> getSpatialGraphsFromContactRelationForInflation(float inflateModelsInMM) const;

    /**
     * @brief getIt
     * @param timestep
     * @return map of shapes at timepoint t
     */
     std::map<double, semrel::ShapeList>::const_iterator getIt(double timestep) const{
        auto it = shapes.find(timestep);
        if (it == shapes.end()) {
            it = shapes.upper_bound(timestep);
        }
        return it;
    }

    /**
     * @brief getMotionName
     * @param id
     * @return motion name (corresponds to node_id)
     */
    std::string getMotionName(int id) const {
        auto it = motionNames.find(id);
        if (it != motionNames.end())
            return it->second;
        else return std::string();
    }
    /**
     * @brief getShapeID
     * @param motionName
     * @return shapeID as int
     */
    int getShapeID(std::string motionName) const {
        for (auto &i : motionNames) {
               if (i.second == motionName) {
                  return i.first;
               }
        }
        throw std::runtime_error("No shape id found for " + motionName );
    }

private:
    /**
     * @brief add box shape when using bounding boxes for contact detection
     * @param node
     * @param id
     * @param visuType
     * @param includeChildNodes
     * @return
     */
    semrel::Shape* getBoxShape(const VirtualRobot::SceneObjectPtr& node, long id, VirtualRobot::SceneObject::VisualizationType visuType = VirtualRobot::SceneObject::Full, bool includeChildNodes = true) const;
    /**
     * @brief add one shape to shape map
     * @param shape
     * @param timestep
     */
    void addShape(semrel::Shape* shape, double timestep);
    /**
     * @brief calculateContactPoints between current objects
     * @param objects
     * @return list of contact points
     */
    semrel::ContactPointList calculateContactPoints(const std::vector<semrel::Shape*> &objects) const;

    /// map including all shapes
    std::map<double, semrel::ShapeList> shapes;
    /// mapping between motion names and shapeID
    std::map<int, std::string> motionNames;
    /// input motion
    MMM::MotionRecordingPtr motions;
    /// delta of timesteps
    double timestepDelta;

};

}

